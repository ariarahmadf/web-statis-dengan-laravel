<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulir Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br>
        <input type="text" name="f_name"><br><br>

        <label>Last name:</label><br>
        <input type="text" name="l_name"><br><br>

        <label>Gender:</label> <br>
        <input type="radio" name="gender" value="male"> Male <br>
        <input type="radio" name="gender" value="female"> Female <br>
        <input type="radio" name="gender" value="other"> Other <br><br>

        <label>Nationality:</label><br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singapuran">Singapuran</option>
            <option value="malaysian">Malaysian</option>
            <option value="autralian">Australian</option>
        </select><br><br>

        <label>Language Spoken:</label><br>
        <input type="checkbox" name="checkbox" value="bahasa indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="checkbox" value="english"> English <br>
        <input type="checkbox" name="checkbox" value="arabic"> Arabic <br>
        <input type="checkbox" name="checkbox" value="japanese"> Japanese <br> <br>

        <label>Bio:</label> <br>
        <textarea name="bio"cols="30" rows="10"></textarea> <br><br>
        <input type="submit"value="Sign Up">

    </form>
</body>
</html>