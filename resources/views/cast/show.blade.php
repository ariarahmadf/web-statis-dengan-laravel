@extends('master')

@section('title')
    Tampilkan Single Data Cast
@endsection

@section('subtitle')
    Detail Cast
@endsection

@section('content')

<h2>Show Cast {{$cast->id}}</h2>
<p>{{$cast->nama}}</p>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>
    
@endsection
