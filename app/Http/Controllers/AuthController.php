<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('form');
    }

    public function signUp(Request $request) {
        $firstName = $request["f_name"];
        $lastName = $request["l_name"];

        return view('welcome', compact('firstName', 'lastName'));
    }
}
